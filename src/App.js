import logo from './logo.svg';
import './App.css';
import  Card from './component/Card';

function App() {
  const data = [
    {id:1 , name: "John Doe", isValid : true},
    {id:2 , name: "John Doe2", isValid : false},
    {id:3 , name: "John Doe3", isValid : true},
  ]
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        {
          data.map((item) => {
            return <Card data={item} />
          })
        }
        // <Card data={data} />
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
