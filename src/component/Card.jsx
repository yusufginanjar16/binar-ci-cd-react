export default function Card({data}) {
    const {name, isValid } = data

    const h1 = <p>{name}</p>
    const elementName = isValid ? h1 : <p>Invalid</p>
    return(
        <div className="card" data-testid="card">
            <div className="card-body">
                <p>Hello World</p>
                {elementName}
            </div>
        </div>
    )
}