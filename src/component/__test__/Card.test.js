import renderer from 'react-test-renderer';
import { render, screen, cleanup } from '@testing-library/react';
import Card from '../Card';

afterEach(() => {
    cleanup();
    });

test("DOM testing", () => {

    const data = {
        id : 1,
        name : "John Doe",
        isValid : true
    }


    render(<Card data={data}/>);
    const cardElement = screen.getByTestId('card');
    expect(cardElement).toBeInTheDocument(); 
    expect(cardElement).toHaveClass('card');
    expect(cardElement).toHaveTextContent('Hello World');
    expect(cardElement).toContainHTML('<p>Hello World</p>');
});

test("Snapshot testing", () => {
    const data = {
        id : 1,
        name : "John Doe",
        isValid : true
    }
    const tree = renderer.create(<Card data={data}/>).toJSON();
    expect(tree).toMatchSnapshot();
});